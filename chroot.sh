#!/usr/bin/env nix-shell
#!nix-shell -p debootstrap fakeroot fakechroot -i bash

fakeroot fakechroot debootstrap --variant=fakechroot focal ubuntu http://archive.ubuntu.com/ubuntu/

read -d '' script << EOF
export PATH="/bin:/sbin"
export TERM=xterm
exec bash
EOF

cargo run --example chroot ubuntu /bin/bash -c "$script"
