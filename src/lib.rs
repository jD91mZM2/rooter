use tempfile::TempDir;
use anyhow::{Context, Result};
use nix::{
    mount::{self, MsFlags},
    sched::CloneFlags,
    unistd,
};
use std::{
    env,
    fs::{self, File},
    os::unix::fs::symlink,
    panic::UnwindSafe,
    path::{Path, PathBuf},
};

mod safe_clone;

use self::safe_clone::safe_clone;


/// Spawn a child process with its own namespace and run rooter in a temporary directory. You will
/// need to specify the `stack_size`, and unfortunately you need to make sure it's big enough - a
/// small stack size may lead to memory corruption.
///
/// TODO: Make this function not be unsafe..........
pub fn with_namespace<F, R>(stack_size: usize, callback: F) -> Result<R>
where
    F: FnOnce(Rooter) -> R + Send + UnwindSafe,
{
    let tempdir = TempDir::new()?;
    let path = tempdir.path();

    let value = safe_clone(
        stack_size,
        CloneFlags::CLONE_NEWNS | CloneFlags::CLONE_NEWUSER,
        Box::new(move || -> Result<R> {
            let rooter = Rooter::new(path.to_owned());
            Ok(callback(rooter))
        }),
    ).context("error when cloning process")??;

    // In the host namespace, remove tempdir after child was completed.
    // I prefer to be explicit about this.
    drop(tempdir);

    Ok(value)
}

fn relative_to(host: &Path, target: &Path) -> PathBuf {
    let relative = target.strip_prefix("/").unwrap_or(target);
    host.join(relative)
}
fn mount(source: &Path, target: &Path, flags: MsFlags) -> Result<()> {
    mount::mount(Some(source), target, Some("none"), MsFlags::MS_BIND | MsFlags::MS_REC | flags, None::<&Path>)?;
    Ok(())
}

/// A new chroot instance which lets you bind host directories
pub struct Rooter {
    root: PathBuf,
    old_root: Option<PathBuf>,
    preserve_cwd: bool,
}
impl Rooter {
    /// Return a new instance of `Rooter` inside a specified root directory.
    ///
    /// NOTE: This will not launch a new namespace. If you run this with the right permissions,
    /// this instance will *actually* replace the root directory for you.
    ///
    /// NOTE: You may prefer the `with_namespace` function which will create a new namespace, and
    /// also give you a rooter instance to a brand new temporary directory.
    pub fn new(root: PathBuf) -> Self {
        Self {
            root,
            old_root: None,
            preserve_cwd: false,
        }
    }

    /// Bind a host file to a directory in the new chroot.
    ///
    /// ```rust
    /// # use std::{fs, path::Path};
    /// # rooter::with_namespace(1024 * 1024, |mut rooter| -> anyhow::Result<()> {
    /// #
    /// // Remember file content from before the chroot
    /// let content = fs::read(file!())?;
    ///
    /// // Bind the source for this file to /testing
    /// rooter.bind_file(file!(), "/testing")?;
    ///
    /// // Perform chroot
    /// rooter.chroot()?;
    ///
    /// // Now /testing is accessible
    /// assert_eq!(fs::read("/testing")?, content);
    /// #
    /// # Ok(())
    /// # }).unwrap().unwrap();
    /// ```
    pub fn bind_file<P1, P2>(&mut self, source: P1, target: P2) -> Result<&mut Self>
    where
        P1: AsRef<Path>,
        P2: AsRef<Path>,
    {
        let source = source.as_ref();
        let target = relative_to(&self.root, target.as_ref());

        if let Some(parent) = target.parent() {
            fs::create_dir_all(parent)?;
        }

        File::create(&target)?;

        mount(source, &target, MsFlags::empty())?;
        Ok(self)
    }

    /// Bind a host directory to a directory in the new chroot.
    ///
    /// ```rust
    /// # use std::{fs, path::Path};
    /// # rooter::with_namespace(1024 * 1024, |mut rooter| -> anyhow::Result<()> {
    /// #
    /// // Bind /home to /new_home
    /// rooter.bind_dir("/home", "/new_home")?;
    /// rooter.chroot()?;
    ///
    /// // Now /new_home exists (and not /home of course)
    /// assert!(Path::new("/new_home").is_dir());
    /// assert!(!Path::new("/home").exists());
    /// #
    /// # Ok(())
    /// # }).unwrap().unwrap();
    /// ```
    pub fn bind_dir<P1, P2>(&mut self, source: P1, target: P2) -> Result<&mut Self>
    where
        P1: AsRef<Path>,
        P2: AsRef<Path>,
    {
        let source = source.as_ref();
        let target = relative_to(&self.root, target.as_ref());

        fs::create_dir_all(&target)?;

        mount(source, &target, MsFlags::empty())?;
        Ok(self)
    }

    /// Create a symlink in the new root.
    ///
    /// - The first argument is where the symlink is placed.
    /// - The second argument is what it should point to.
    ///
    /// ```rust
    /// # use std::{fs, path::Path};
    /// # rooter::with_namespace(1024 * 1024, |mut rooter| -> anyhow::Result<()> {
    /// # 
    /// // Bind /proc to /dir
    /// rooter.bind_dir("/proc", "/dir")?;
    ///
    /// // In the new system, symlink /link to our /dir bind.
    /// rooter.symlink("/link", "/dir")?;
    ///
    /// // Perform chroot
    /// rooter.chroot()?;
    ///
    /// // Now /link is a symlink to our /dir bind!
    /// assert_eq!(fs::read_link("/link")?, Path::new("/dir"));
    /// #
    /// # Ok(())
    /// # }).unwrap().unwrap();
    /// ```
    pub fn symlink<P1, P2>(&mut self, file: P1, target: P2) -> Result<&mut Self>
    where
        P1: AsRef<Path>,
        P2: AsRef<Path>,
    {
        let file = relative_to(&self.root, file.as_ref());
        let target = target.as_ref();

        if let Some(parent) = file.parent() {
            fs::create_dir_all(parent)?;
        }

        symlink(target, &file)?;

        Ok(self)
    }

    /// Mirror a file, directory, or symlink in the new root. Files and directories are mounted
    /// with --bind. Symlinks are simply copied.
    ///
    /// ```rust
    /// # use std::{env, fs, path::Path};
    /// # rooter::with_namespace(1024 * 1024, |mut rooter| -> anyhow::Result<()> {
    /// #
    /// // Remember content of this source file before chroot
    /// let content = fs::read(file!())?;
    ///
    /// // Bind a directory
    /// rooter.bind("/proc", "/directory")?;
    ///
    /// // Bind a file
    /// rooter.bind(file!(), "/file")?;
    ///
    /// // Bind a symlink (will just be copied)
    /// # env::set_current_dir("/")?;
    /// rooter.bind("/proc/self/cwd", "/link")?;
    ///
    /// // Perform chroot
    /// rooter.chroot()?;
    ///
    /// // Our directory still exists
    /// assert!(Path::new("/directory").is_dir());
    ///
    /// // Our file is intact
    /// assert_eq!(fs::read("/file")?, content);
    ///
    /// // Our link was mirrored
    /// assert_eq!(fs::read_link("/link")?, env::current_dir()?);
    /// #
    /// # Ok(())
    /// # }).unwrap().unwrap();
    /// ```
    pub fn bind<P1, P2>(&mut self, source: P1, target: P2) -> Result<&mut Self>
    where
        P1: AsRef<Path>,
        P2: AsRef<Path>,
    {
        let source = source.as_ref();
        let target = target.as_ref();

        let kind = fs::symlink_metadata(source)?.file_type();

        if kind.is_dir() {
            self.bind_dir(source, target).context("failed to bind directory")?;
        } else if kind.is_file() {
            self.bind_file(source, target).context("failed to bind file")?;
        } else if kind.is_symlink() {
            let link_target = fs::read_link(source).context("failed to mirror symlink")?;
            self.symlink(target, link_target)?;
        } else {
            unimplemented!("bind(...) not implemented for this filetype");
        }

        Ok(self)
    }

    /// Passthrough a host directory into the new root. This is equivalent to
    /// ```ascii
    /// rooter.bind(source, source)
    /// ```
    ///
    /// ```rust
    /// # use std::{fs, path::Path};
    /// # rooter::with_namespace(1024 * 1024, |mut rooter| -> anyhow::Result<()> {
    /// #
    /// // Transfer important directories into chroot
    /// rooter
    ///     .bind_self("/dev")?
    ///     .bind_self("/proc")?
    ///     .bind_self("/sys")?;
    ///
    /// // Perform chroot
    /// rooter.chroot()?;
    ///
    /// // Collect names of all directories in the new root
    /// let mut dirs: Vec<_> = fs::read_dir("/")?
    ///     .map(Result::unwrap)
    ///     .map(|dirent| dirent.path())
    ///     .collect();
    /// let mut strings: Vec<_> = dirs.iter()
    ///     .map(|path| path.to_str())
    ///     .collect();
    /// strings.sort();
    ///
    /// // See, we got exactly what we bound!
    /// assert_eq!(strings, [
    ///     Some("/dev"), 
    ///     Some("/proc"),
    ///     Some("/sys")
    /// ]);
    /// #
    /// # Ok(())
    /// # }).unwrap().unwrap();
    /// ```
    pub fn bind_self<P>(&mut self, source: P) -> Result<&mut Self>
    where
        P: AsRef<Path>,
    {
        let source = source.as_ref();
        self.bind(source, source)
    }

    /// Bind the old root directory to a new directory inside of the chroot.
    ///
    /// ```rust
    /// # use std::{fs, path::Path};
    /// # rooter::with_namespace(1024 * 1024, |mut rooter| -> anyhow::Result<()> {
    /// #
    /// // Bind old root as /host
    /// rooter.bind_root("/host")?;
    /// rooter.chroot()?;
    ///
    /// // We can access stuff in /host
    /// assert!(Path::new("/host/proc").is_dir());
    /// #
    /// # Ok(())
    /// # }).unwrap().unwrap();
    /// ```
    pub fn bind_root<P>(&mut self, path: P) -> Result<&mut Self>
    where
        P: Into<PathBuf>,
    {
        self.old_root = Some(path.into());
        Ok(self)
    }

    /// A chainable function that decides whether or not to preserve current working directory. If
    /// you don't enable this, and don't add your own cwd handling, env::current_dir() will fail
    /// spuriously.
    ///
    /// Default: `false`
    ///
    /// ```rust
    /// # use std::{env, fs, path::Path};
    /// # rooter::with_namespace(1024 * 1024, |mut rooter| -> anyhow::Result<()> {
    /// #
    /// // Set current directory to /proc
    /// env::set_current_dir("/proc")?;
    ///
    /// // Bind /proc and enable preserve_cwd
    /// rooter
    ///     .bind_self("/proc")?;
    ///     .preserve_cwd(true);
    ///
    /// // Perform chroot
    /// rooter.chroot()?;
    ///
    /// // Check our cwd
    /// assert_eq!(&env::current_dir()?, Path::new("/proc"));
    /// #
    /// # Ok(())
    /// # }).unwrap().unwrap();
    /// ```
    pub fn preserve_cwd(&mut self, setting: bool) -> &mut Self {
        self.preserve_cwd = setting;
        self
    }

    /// After the root is set up, perform the final `chroot` needed to enter it. This will NOT
    /// create a new namespace - use `with_namespace` for that.
    ///
    /// Unless `preserve_cwd` is set to true, this will set cwd to /.
    ///
    /// Behind the scenes, a `pivot_root` is used if a `bind_root` call was made, otherwise a
    /// simple `chroot`.
    pub fn chroot(self) -> Result<()> {
        let old_cwd = if self.preserve_cwd {
            Some(env::current_dir().context("failed getting data to preserve cwd")?)
        } else {
            None
        };

        if let Some(ref old_root) = self.old_root {
            // Create new mountpoint for old root directory
            let old_root_real = relative_to(&self.root, old_root);
            fs::create_dir_all(&old_root_real)?;

            // Root must be a mount point - bind it to itself.
            // Mount must not be a shared mount (so, MS_PRIVATE)
            mount(&self.root, &self.root, MsFlags::MS_PRIVATE)?;

            // Swap roots
            unistd::pivot_root(&self.root, &old_root_real)?;
        } else {
            unistd::chroot(&self.root)?;
        }

        match old_cwd {
            None => env::set_current_dir("/").context("failed to reset cwd")?,
            Some(cwd) => env::set_current_dir(cwd).context("failed to preserve cwd")?,
        }

        Ok(())
    }
}
