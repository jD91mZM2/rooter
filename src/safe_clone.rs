use anyhow::{Context, Result};
use nix::{
    libc::{self, c_void, c_int},
    sched::CloneFlags,
    sys::wait::{waitpid, WaitStatus},
    unistd::Pid,
};

use std::{
    panic::{self, UnwindSafe},
    process,
    sync::mpsc,
};

/// A safe and more idiomatic version of libc's `clone`. Lifetime safety is ensured by not letting the clone
/// escape this function. Thus, it's not as flexible as a real clone as you cannot do anything
/// while its running.
///
/// The return value is propagated back to the main process. This is done using shared memory, so
/// CLONE_VM is a hardcoded flag. This function returns only when the clone returns.
///
/// NOTE: If `stack_size` is too small, the child may try to write outside of its bounds. Stack
/// overflows are currently not caught in Rust, so I don't count it as `unsafe`. But it's
/// definitely a real problem which I hope a future version of libc will solve.
///
/// TODO: Extract into its own library? I can see this being somewhat useful in some other cases.
pub fn safe_clone<'a, F, R>(stack_size: usize, flags: CloneFlags, callback: F) -> Result<R>
where
    F: FnOnce() -> R + Send + UnwindSafe + 'a,
{
    extern "C" fn clone_callback(data: *mut c_void) -> c_int {
        let closure: Box<dyn FnOnce()> = unsafe { Box::from_raw(data as *mut _ as *mut Box<dyn FnOnce()>) };
        closure();
        0
    }

    let (tx, rx) = mpsc::sync_channel(1);

    let closure: Box<Box<dyn FnOnce()>> = Box::new(Box::new(move || {
        match panic::catch_unwind(callback) {
            Ok(value) => {
                tx.send(value).expect("safe_clone: mpsc channel send() failed");
                process::exit(0);
            },
            Err(err) => {
                eprintln!("PANIC! {:?}", err);
                process::abort();
            },
        }
    }));

    let mut stack = vec![0u8; stack_size];

    let pid = unsafe {
        let stack_end = stack.as_mut_ptr().add(stack_size - 1);
        let stack_end_aligned = stack_end.sub(stack_end as usize % 16);
        libc::clone(
            clone_callback,
            stack_end_aligned as *mut c_void,
            libc::CLONE_VM | libc::SIGCHLD | flags.bits(),
            Box::into_raw(closure) as *mut _ as *mut c_void,
        )
    };

    if pid == -1 {
        return Err(nix::Error::last().into());
    }

    let value = rx.recv().context("waiting for child process")?;

    loop {
        match waitpid(Pid::from_raw(pid), None).context("waiting for child process")? {
            WaitStatus::StillAlive => (),
            _ => break,
        }
    }

    // Drop stack once used
    drop(stack);

    Ok(value)
}
