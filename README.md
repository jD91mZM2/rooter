# rooter

An idiomatic library to create all kinds of chroots using Linux namespaces. Yay!

## Examples

The `chroot` example is a real and working chroot implementation that does not
require root. It will bind the host directory as `/host` by default.
