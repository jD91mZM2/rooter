//! A simple `chroot` alternative that does not require root privileges :)

use rooter::*;

use anyhow::anyhow;

use std::{
    env,
    fs,
    path::Path,
    process::Command,
};

fn main() -> anyhow::Result<()> {
    with_namespace(1024 * 1024, |mut rooter| -> anyhow::Result<()> {
        let usage = "Usage: chroot <path> <command> [args...]";

        let mut args = env::args().skip(1);
        let dir = args.next().ok_or_else(|| anyhow!(usage))?;
        let command = args.next().ok_or_else(|| anyhow!(usage))?;

        rooter
            .bind_self("/dev")?
            .bind_self("/proc")?
            .bind_self("/sys")?
            .bind_root("/host")?;

        // Because `with_namespace` creates a new empty temporary directory instead of being
        // related to `dir`, we need to bind each directory from `dir` onto our rooter instance.
        //
        // (We could make a `Rooter::new(dir)` but meh work. Also it wouldn't get cleaned up
        // automagically)
        for dirent in fs::read_dir(dir)? {
            let dirent = dirent?;

            let real = dirent.path();
            let fake = Path::new("/").join(dirent.file_name());

            match fake.to_str() {
                Some("/dev") |
                Some("/proc") |
                Some("/sys") |
                Some("/host") => continue,
                _ => (),
            }

            rooter.bind(real, fake)?;
        }

        rooter.chroot()?;

        Command::new(command)
            .args(args)
            .status()?;

        Ok(())
    })??;
    Ok(())
}
